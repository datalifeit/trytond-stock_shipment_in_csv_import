# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from io import StringIO
from trytond.model import fields, ModelView
from trytond.model.fields import Many2One
from trytond.pool import PoolMeta, Pool
from trytond.pyson import PYSONEncoder
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateAction, Button
import csv
import ast

__all__ = ['CsvImport', 'CsvImportStart', 'Configuration']


class CsvImport(Wizard):
    """Shipment in CSV Import"""
    __name__ = 'stock.shipment.in.csv_import'

    @classmethod
    def __setup__(cls):
        super(CsvImport, cls).__setup__()
        cls._error_messages.update({
            'product_not_found': 'Product "%s" not found.',
            'supplier_not_found': 'Supplier "%s" not found.',
        })

    start = StateView(
        'stock.shipment.in.csv_import.start',
        'stock_shipment_in_csv_import.csv_import_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Import', 'import_', 'tryton-ok', default=True),
        ])

    import_ = StateAction('stock.act_shipment_in_form')

    def do_import_(self, action):
        pool = Pool()
        Config = pool.get('stock.configuration')
        config = Config(1)
        Shipment = pool.get('stock.shipment.in')
        Move = pool.get('stock.move')
        Company = pool.get('company.company')
        shipments = []
        to_del = []

        config_header = []
        if config.csv_headers:
            config_header = ast.literal_eval(config.csv_headers)

        csv_file = self.start.csv_file

        # On python3 we must convert the binary file to string
        if hasattr(csv_file, 'decode'):
            csv_file = csv_file.decode(errors='ignore')
        csv_file = StringIO(csv_file)
        csv_file = csv.reader(csv_file, delimiter=',')
        headers = next(csv_file)

        unique_key = dict([
            (d, i) for i, d in enumerate(config_header)
            if d in self._get_csv_key(config_header)])
        data = {}
        defvalues = {}

        assert len(headers) == len(config_header)
        for row in csv_file:
            _key = tuple(row[value] for value in sorted(
                unique_key.values()))
            if not data.get(_key):
                data.setdefault(_key, {'incoming_moves': []})
            data[_key]['incoming_moves'].append({})
            for i, column in enumerate(config_header):
                if column.startswith('incoming_moves'):
                    _, column = column.split('.')
                    data[_key]['incoming_moves'][-1][column] = row[i]
                else:
                    row[i] = self.get_value(Shipment, column, row[i], defvalues)
                    data[_key][column] = row[i]

        company = Company(Transaction().context['company'])
        for k, v in data.items():
            shipment = None
            if k[0]:
                shipment = Shipment.search([
                    ('company', '=', company.id),
                    ('reference', '=', k[0]),
                    ('supplier.name', '=', k[1])
                ], limit=1)

                if shipment:
                    shipment = shipment[0]
                    if shipment.state != 'draft':
                        continue
                    if v['incoming_moves'][0]:
                        to_del.extend(list(shipment.incoming_moves))

            if not shipment:
                shipment = Shipment()

            moves = []
            _move_data = v.pop('incoming_moves')
            for k2, v2 in v.items():
                setattr(shipment, k2, v2)
            if not shipment.supplier:
                self.raise_user_error('supplier_not_found',
                    v.get('supplier', ''))
            for move_values in _move_data:
                if not move_values:
                    continue
                move = Move(
                    company=company,
                    currency=company.currency,
                    effective_date=shipment.effective_date)
                for move_key, move_value in move_values.items():
                    _value = self.get_value(Move, move_key, move_value, defvalues)
                    setattr(move, move_key, _value)
                move.from_location = shipment.on_change_with_supplier_location()
                move.to_location = shipment.on_change_with_warehouse_input()
                move.quantity = float(move.quantity)
                if not move.product:
                    self.raise_user_error('product_not_found',
                        move_values.get('product', ''))
                move.on_change_product()
                moves.append(move)
            shipment.incoming_moves = moves
            shipments.append(shipment)

        if shipments:
            Shipment.save(shipments)
        if to_del:
            Move.delete(to_del)

        action['pyson_domain'] = PYSONEncoder().encode([
            ('id', 'in', list(map(int, shipments))),
        ])
        return action, {}

    @classmethod
    def _get_csv_key(cls, config_header):
        return ('reference', 'supplier', 'warehouse', 'effective_date')

    @classmethod
    def get_value(cls, Model, column, value, defvalues):
        pool = Pool()
        if isinstance(Model._fields[column], Many2One):
            if value in defvalues.get(column, {}):
                return defvalues[column][value]
            field = Model._fields[column]
            relation = field.get_target().__name__
            Model = pool.get(relation)
            model = Model.search([('name', '=', value)])
            if not model:
                res = None
            else:
                res = model[0]
            defvalues.setdefault(column, {})[value] = res
            return res
        return value


class CsvImportStart(ModelView):
    """Import CSV View"""
    __name__ = 'stock.shipment.in.csv_import.start'

    csv_file = fields.Binary('File', required=True)


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    csv_headers = fields.Char('CSV Headers')
