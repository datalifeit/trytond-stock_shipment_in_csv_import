# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .stock import CsvImportStart, CsvImport, Configuration


def register():
    Pool.register(
        CsvImportStart,
        Configuration,
        module='stock_shipment_in_csv_import', type_='model')
    Pool.register(
        CsvImport,
        module='stock_shipment_in_csv_import', type_='wizard')
    Pool.register(
        module='stock_shipment_in_csv_import', type_='report')
